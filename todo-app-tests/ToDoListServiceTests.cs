﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using todo_aspnetmvc.Services;
using todo_domain_entities;
using todo_domain_entities.Models;

namespace todo_app_tests
{
    [TestFixture]
    public class ToDoListServiceTests
    {
        static DbContextOptions<AppDbContext> options = new DbContextOptionsBuilder<AppDbContext>()
            .UseInMemoryDatabase(databaseName: "ToDoList")
            .Options;

        private AppDbContext _appDbContext;
        private IToDoListService _toDoListsService;

        [SetUp]
        public void SetUp()
        {
            _appDbContext = new AppDbContext(options);
            _appDbContext.Database.EnsureDeleted();
            _toDoListsService = new ToDoListService(_appDbContext);
        }

        [Test]
        public async Task GetToDoLists_ShouldReturnAllLists()
        {
            await _toDoListsService.Create(new ToDoListModel
            {
                Id = Guid.NewGuid(), Name = "List 1", IsHidden = false, Icon = "img/no-pictures.png"
            });
            await _toDoListsService.Create(new ToDoListModel
            {
                Id = Guid.NewGuid(), Name = "List 2", IsHidden = false, Icon = "img/calendar.png"
            });

            var result = await _toDoListsService.GetToDoLists(new ItemFilterParameters {IsHidden = false});
            Assert.That(result, Has.Count.EqualTo(2));
        }

        [Test]
        public async Task GetToDoLists_ShouldReturnListById()
        {
            var listId = Guid.NewGuid();
            await _toDoListsService.Create(new ToDoListModel
            {
                Id = listId, Name = "List 1", IsHidden = false, Icon = "img/no-pictures.png"
            });

            var result = await _toDoListsService.GetToDoList(listId);
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Id, Is.EqualTo(listId));
        }

        [Test]
        public async Task Update_ShouldUpdateListById()
        {
            var listId = Guid.NewGuid();
            await _toDoListsService.Create(new ToDoListModel
            {
                Id = listId, Name = "List 1", IsHidden = false, Icon = "img/no-pictures.png"
            });
            var list = await _toDoListsService.GetToDoList(listId);

            list.Name = "List 2";
            await _toDoListsService.Update(listId, list);

            var result = await _toDoListsService.GetToDoList(listId);

            Assert.That(result.Name, Is.EqualTo("List 2"));
        }

        [Test]
        public async Task CreateList_ShouldCreateNewList()
        {
            var listId = Guid.NewGuid();
            await _toDoListsService.Create(new ToDoListModel
            {
                Id = listId, Name = "List 1", IsHidden = false, Icon = "img/no-pictures.png"
            });

            var result = await _toDoListsService.GetToDoList(listId);
            Assert.That(result.Id, Is.EqualTo(listId));
        }

        [Test]
        public async Task Delete_ShouldDeleteListById()
        {
            var listId = Guid.NewGuid();
            await _toDoListsService.Create(new ToDoListModel
            {
                Id = listId, Name = "List 1", IsHidden = false, Icon = "img/no-pictures.png"
            });

            var result = await _toDoListsService.GetToDoList(listId);
            Assert.That(result, Is.Not.Null);

            await _toDoListsService.Delete(listId);
            result = await _toDoListsService.GetToDoList(listId);
            Assert.That(result, Is.Null);
        }
    }
}