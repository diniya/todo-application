﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using todo_aspnetmvc.Services;
using todo_domain_entities;
using todo_domain_entities.Models;

namespace todo_app_tests
{
    [TestFixture]
    public class ItemsServiceTests
    {
        static DbContextOptions<AppDbContext> options = new DbContextOptionsBuilder<AppDbContext>()
            .UseInMemoryDatabase(databaseName: "ToDoList")
            .Options;

        private AppDbContext _appDbContext;
        private IItemService _itemsService;
        private IToDoListService _toDoListsService;

        [SetUp]
        public void SetUp()
        {
            _appDbContext = new AppDbContext(options);
            _appDbContext.Database.EnsureDeleted();
            _itemsService = new ItemService(_appDbContext);
            _toDoListsService = new ToDoListService(_appDbContext);
        }

        [Test]
        public async Task GetItems_ShouldReturnAllItemsByListId()
        {
            var listId = Guid.NewGuid();

            await _toDoListsService.Create(new ToDoListModel
            {
                Id = listId, Name = "List 1", IsHidden = false, Icon = "img/no-pictures.png"
            });

            await _itemsService.Create(new ItemModel
            {
                Id = Guid.NewGuid(),
                Name = "Task 1",
                Description = "task 1 for List 1",
                Status = StatusItem.NotStarted,
                StartDate = DateTime.Now,
                CompletionDate = DateTime.Now.AddDays(2),
                ToDoListId = listId
            });

            await _itemsService.Create(new ItemModel
            {
                Id = Guid.NewGuid(),
                Name = "Task 2",
                Description = "task 2 for List 1",
                Status = StatusItem.NotStarted,
                StartDate = DateTime.Now,
                CompletionDate = DateTime.Now.AddDays(2),
                ToDoListId = listId
            });
            var result = await _itemsService.GetToDoListItems(listId, new ItemFilterParameters());
            Assert.That(result, Has.Count.EqualTo(2));
        }

        [Test]
        public async Task GetItem_ShouldReturnItemByTaskId()
        {
            var listId = Guid.NewGuid();
            await _toDoListsService.Create(new ToDoListModel
            {
                Id = listId, Name = "List 1", IsHidden = false, Icon = "img/no-pictures.png"
            });

            var taskId = Guid.NewGuid();
            await _itemsService.Create(new ItemModel
            {
                Id = taskId,
                Name = "Task 1",
                Description = "task 1 for List 1",
                Status = StatusItem.NotStarted,
                StartDate = DateTime.Now,
                CompletionDate = DateTime.Now.AddDays(2),
                ToDoListId = listId
            });

            var result = await _itemsService.GetItem(taskId);
            Assert.That(result, Is.Not.Null);
            Assert.That(taskId, Is.EqualTo(result.Id));
        }

        [Test]
        public async Task CreateItem_ShouldCreateNewItem()
        {
            var listId = Guid.NewGuid();
            await _toDoListsService.Create(new ToDoListModel
            {
                Id = listId, Name = "List 1", IsHidden = false, Icon = "img/no-pictures.png"
            });

            var taskId = Guid.NewGuid();
            await _itemsService.Create(new ItemModel
            {
                Id = taskId,
                Name = "Task 1",
                Description = "task 1 for List 1",
                Status = StatusItem.NotStarted,
                StartDate = DateTime.Now,
                CompletionDate = DateTime.Now.AddDays(2),
                ToDoListId = listId
            });

            var result = await _itemsService.GetItem(taskId);
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Id, Is.EqualTo(taskId));
        }

        [Test]
        public async Task UpdateItem_ShouldUpdateItem()
        {
            var listId = Guid.NewGuid();
            await _toDoListsService.Create(new ToDoListModel
            {
                Id = listId, Name = "List 1", IsHidden = false, Icon = "img/no-pictures.png"
            });

            var taskId = Guid.NewGuid();
            await _itemsService.Create(new ItemModel
            {
                Id = taskId,
                Name = "Task 1",
                Description = "task 1 for List 1",
                Status = StatusItem.NotStarted,
                StartDate = DateTime.Now,
                CompletionDate = DateTime.Now.AddDays(2),
                ToDoListId = listId
            });

            var task = await _itemsService.GetItem(taskId);
            task.Name = "New name Task 2";

            await _itemsService.Update(task);

            var result = await _itemsService.GetItem(taskId);

            Assert.That(result.Name, Is.EqualTo("New name Task 2"));
        }

        [Test]
        public async Task DeleteItem_ShouldDeleteItem()
        {
            var listId = Guid.NewGuid();
            await _toDoListsService.Create(new ToDoListModel
            {
                Id = listId, Name = "List 1", IsHidden = false, Icon = "img/no-pictures.png"
            });

            var taskId = Guid.NewGuid();
            await _itemsService.Create(new ItemModel
            {
                Id = taskId,
                Name = "Task 1",
                Description = "task 1 for List 1",
                Status = StatusItem.NotStarted,
                StartDate = DateTime.Now,
                CompletionDate = DateTime.Now.AddDays(2),
                ToDoListId = listId
            });

            var task = await _itemsService.GetItem(taskId);
            Assert.That(task, Is.Not.Null);

            await _itemsService.Delete(taskId);
            var result = await _itemsService.GetItem(taskId);

            Assert.That(result, Is.Null);
        }
    }
}