﻿using Microsoft.EntityFrameworkCore;
using todo_domain_entities.Models;

namespace todo_domain_entities
{
    public class AppDbContext: DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options) { }
        public DbSet<ToDoListModel> ToDoLists { get; set; }
        public DbSet<ItemModel> ToDoListItems { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ToDoListModel>()
                .HasKey(x=>x.Id);
            modelBuilder.Entity<ToDoListModel>()
                .Property(x=>x.IsHidden).HasDefaultValue(false);

            modelBuilder.Entity<ItemModel>().HasKey(x=>x.Id);
            modelBuilder.Entity<ItemModel>()
                .Property(x=>x.Status).HasDefaultValue(StatusItem.NotStarted);
            
            modelBuilder.Entity<ToDoListModel>()
                .HasMany(x => x.Items)
                .WithOne(x => x.ToDoList)
                .HasForeignKey(x=>x.ToDoListId)
                .OnDelete(DeleteBehavior.Cascade);
        }

    }
}