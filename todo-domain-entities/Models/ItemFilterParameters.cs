﻿using System;

namespace todo_domain_entities.Models
{
    public class ItemFilterParameters : SortParameters
    {
        public string? Name { get; set; }
        public bool IsHidden { get; set; }
        public StatusItem? Status { get; set; }
        public DateTime? StartDateFrom { get; set; }
        public DateTime? StartDateTo { get; set; }
        public DateTime? CompletionDateFrom { get; set; }
        public DateTime? CompletionDateTo { get; set; }
    }
}