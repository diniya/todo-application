﻿using System;
using System.ComponentModel.DataAnnotations;

namespace todo_domain_entities.Models
{
    public class ItemModel
    {
        public Guid Id { get; set; }
        
        [Required]
        public string Name { get; set; }
        public string? Description { get; set; }
        public StatusItem Status { get; set; }
        public DateTime? StartDate { get; set; }
       
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime? CompletionDate { get; set; }
        
        public Guid ToDoListId { get; set; }
        public ToDoListModel? ToDoList { get; set; }
    }
}