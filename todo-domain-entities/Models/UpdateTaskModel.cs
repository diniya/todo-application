﻿using System;

namespace todo_domain_entities.Models
{
    public class UpdateTaskModel
    {
        public Guid ToDoListId { get; set; }
        public StatusItem Status { get; set; }
    }
}