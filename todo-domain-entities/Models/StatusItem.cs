﻿namespace todo_domain_entities.Models
{
    public enum StatusItem
    {
        NotStarted = 0,
        Completed
    }
}