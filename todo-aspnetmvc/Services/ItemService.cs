﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using todo_domain_entities;
using todo_domain_entities.Models;

namespace todo_aspnetmvc.Services
{
    public class ItemService : IItemService
    {
        private readonly AppDbContext _dbContext;

        public ItemService(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<List<ItemModel>> GetToDoListItems(Guid listId, ItemFilterParameters filterParameters)
        {
            var itemsQuery = _dbContext.ToDoListItems
                .Where(x => x.ToDoListId == listId)
                .AsQueryable();
            if (filterParameters.Status.HasValue)
            {
                itemsQuery = itemsQuery
                    .Where(x => x.Status == filterParameters.Status.Value);
            }

            if (filterParameters.CompletionDateFrom.HasValue)
            {
                itemsQuery = itemsQuery
                    .Where(x => x.CompletionDate >= filterParameters.CompletionDateFrom.Value);
            }

            if (filterParameters.CompletionDateTo.HasValue)
            {
                itemsQuery = itemsQuery
                    .Where(x => x.CompletionDate <= filterParameters.CompletionDateTo.Value);
            }

            if (filterParameters.StartDateFrom.HasValue)
            {
                itemsQuery = itemsQuery
                    .Where(x => x.StartDate >= filterParameters.StartDateFrom.Value);
            }

            if (filterParameters.StartDateTo.HasValue)
            {
                itemsQuery = itemsQuery
                    .Where(x => x.StartDate <= filterParameters.StartDateTo.Value);
            }

            if (!string.IsNullOrEmpty(filterParameters.Name))
            {
                itemsQuery = itemsQuery
                    .Where(x => x.Name.Contains(filterParameters.Name));
            }

            return await itemsQuery.ToListAsync();
        }

        public async Task<ItemModel> GetItem(Guid itemId)
        {
            return await _dbContext.ToDoListItems
                .Where(x => x.Id == itemId)
                .FirstOrDefaultAsync();
        }

        public async Task<Guid> Create(ItemModel model)
        {
            model.StartDate = DateTime.Now;
            _dbContext.ToDoListItems.Add(model);
            await _dbContext.SaveChangesAsync();
            return model.Id;
        }

        public async Task Update(ItemModel model)
        {
            _dbContext.ToDoListItems.Update(model);
            await _dbContext.SaveChangesAsync();
        }

        public async Task UpdateStatus(Guid itemId, StatusItem status)
        {
            ItemModel entity = await _dbContext.ToDoListItems
                .FirstOrDefaultAsync(x => x.Id == itemId);
            if (entity == null)
            {
                throw new ArgumentException("Not found");
            }

            entity.Status = status;

            _dbContext.ToDoListItems.Update(entity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task Delete(Guid itemId)
        {
            ItemModel findTask = await _dbContext.ToDoListItems
                .FirstOrDefaultAsync(x => x.Id == itemId);
            if (findTask == null)
            {
                return;
            }

            _dbContext.ToDoListItems.Remove(findTask);
            await _dbContext.SaveChangesAsync();
        }
    }
}