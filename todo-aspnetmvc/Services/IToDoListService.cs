﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using todo_domain_entities.Models;

namespace todo_aspnetmvc.Services
{
    public interface IToDoListService
    {
        /// <summary>
        /// Returns list of todolists
        /// </summary>
        /// <param name="filterParameters">Sorting and filtering parameters</param>
        /// <returns></returns>
        Task<List<ToDoListModel>> GetToDoLists(ItemFilterParameters filterParameters); 
        
        /// <summary>
        /// Returns ToDoList by Id
        /// </summary>
        /// <param name="id">Id of list</param>
        /// <returns></returns>
        Task<ToDoListModel> GetToDoList(Guid id); 
        
        /// <summary>
        /// Creates new ToDoList
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<Guid> Create(ToDoListModel model); 
        
        /// <summary>
        /// Updates ToDoList
        /// </summary>
        /// <param name="listId"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        Task Update(Guid listId,ToDoListModel model); 
        
        /// <summary>
        /// Deletes to do list by Id
        /// </summary>
        /// <param name="id">ListId</param>
        /// <returns></returns>
        Task Delete(Guid id); 
    }
}