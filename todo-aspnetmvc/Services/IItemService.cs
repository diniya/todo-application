﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using todo_domain_entities.Models;

namespace todo_aspnetmvc.Services
{
    public interface IItemService
    {
        /// <summary>
        /// Returns item by Id
        /// </summary>
        /// <param name="itemId">Id of item</param>
        /// <returns></returns>
        Task<ItemModel> GetItem(Guid itemId);

        /// <summary>
        /// Creates new item
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<Guid> Create(ItemModel model);

        /// <summary>
        /// Updates item
        /// </summary>
       /// <param name="model"></param>
        /// <returns></returns>
        Task Update(ItemModel model);

        /// <summary>
        /// Updates item
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        Task UpdateStatus(Guid itemId, StatusItem status);
        
        /// <summary>
        /// Deletes item by Id
        /// </summary>
        /// <param name="itemId">Id of item</param>
        /// <returns></returns>
        Task Delete(Guid itemId);

        /// <summary>
        /// Returns items by listId
        /// </summary>
        /// <param name="listId">id of list</param>
        /// <param name="filterParameters"></param>
        /// <returns></returns>
        Task<List<ItemModel>> GetToDoListItems(Guid listId, ItemFilterParameters filterParameters);
    }
}