﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using todo_domain_entities;
using todo_domain_entities.Models;

namespace todo_aspnetmvc.Services
{
    public class ToDoListService : IToDoListService
    {
        private readonly AppDbContext _dbContext;

        public ToDoListService(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<List<ToDoListModel>> GetToDoLists(ItemFilterParameters filterParameters)
        {
            var toDoListsQuery =  _dbContext
                .ToDoLists
                .OrderBy(x=>x.Name)
                .Include(x => x.Items)
                .AsQueryable();

            toDoListsQuery = toDoListsQuery
                .Where(x => x.IsHidden==filterParameters.IsHidden);

            if (!string.IsNullOrEmpty(filterParameters.Name))
            {
                toDoListsQuery = toDoListsQuery
                    .Where(x => x.Name.Contains(filterParameters.Name));
            }

            return await toDoListsQuery.ToListAsync();
        }

        public async Task<ToDoListModel> GetToDoList(Guid id)
        {
            return await _dbContext
                .ToDoLists
                .Include(x=>x.Items)
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<Guid> Create(ToDoListModel model)
        {
            _dbContext.ToDoLists.Add(model);
            await _dbContext.SaveChangesAsync();
            return model.Id;
        }

        public async Task Update(Guid listId, ToDoListModel model)
        {
            ToDoListModel entity = await _dbContext.ToDoLists
                .FirstOrDefaultAsync(x => x.Id == listId);
            if (entity == null)
            {
                throw new ArgumentException("Not found");
            }

            entity.Name = model.Name;
            entity.Icon = model.Icon;
            entity.IsHidden = model.IsHidden;

            _dbContext.ToDoLists.Update(entity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task Delete(Guid id)
        {
            ToDoListModel findProject = await _dbContext.ToDoLists
                .FirstOrDefaultAsync(x => x.Id == id);
            if (findProject == null)
            {
                return;
            }

            _dbContext.ToDoLists.Remove(findProject);
            await _dbContext.SaveChangesAsync();
        }
    }
}