﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using todo_aspnetmvc.Services;
using todo_domain_entities.Models;

namespace todo_aspnetmvc.Controllers
{
    public class ToDoListController : Controller
    {
        private readonly IToDoListService _toDoListsService;

        public ToDoListController(IToDoListService toDoListsService)
        {
            _toDoListsService = toDoListsService;
        }

        [HttpGet]
        public async Task<IActionResult> Index(bool isHidden = false)
        {
            var toDoList = await _toDoListsService.GetToDoLists(new ItemFilterParameters {IsHidden = isHidden});

            return View(toDoList);
        }


        [HttpGet("{listId:guid}")]
        public async Task<IActionResult> Get([FromRoute] Guid listId)
        {
            ToDoListModel findTask = await _toDoListsService.GetToDoList(listId);
            return View(findTask);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(Guid? listId)
        {
            if (listId.HasValue)
            {
                ToDoListModel findTask = await _toDoListsService.GetToDoList(listId.Value);
                return View(findTask);
            }

            return View(new ToDoListModel());
        }

        [HttpPost]
        public async Task<IActionResult> Save([FromForm] ToDoListModel list)
        {
            if (!ModelState.IsValid)
            {
                return View("Edit", list);
            }
            
            if (list.Id == Guid.Empty)
            {
                list.Id = await _toDoListsService.Create(list);
            }
            else
            {
                await _toDoListsService.Update(list.Id, list);
            }

            return RedirectToAction("Get", "ToDoList", new {listId = list.Id});
        }

        [HttpPost("{listId:guid}")]
        public async Task<IActionResult> DeleteList([FromRoute] Guid listId)
        {
            await _toDoListsService.Delete(listId);
            return RedirectToAction("Index", "ToDoList");
        }
    }
}